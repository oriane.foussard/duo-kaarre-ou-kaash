# Duo kaarré ou kaash

Projet Informatique de deuxième année  de l'ENSAI (2021-2022).

# Présentation

Pour nombre de séries télévisées, il y a des communautés de fans qui ont constitué des fonds de citations ainsi que des api permettant de les récupérer.
L'objectif de ce projet va être de se servir de ces api afin de récupérer des données, créer des questions et réaliser une application de type "quizz".
L'exemple proposé est la série Kaamelott de l'[API](https://kaamelott.chaudie.re/), dont son fonctionnement est expliqué sur ce [GitHub](https://github.com/sin0light/api-kaamelott/).


# Fonctionnalités de base pour l'utilisateur

- choix de l'utilisateur répondant aux questions à l'entrée sur l'application 
- série de questions :
    - l'utilisateur choisit à son entrée le nombre de questions auxquelles il devra répondre
    - l'utilisateur choisit avant de répondre duo, kaarré ou kaash : chaque choix rapportant un nombre de point différent
- sauvegarde des scores réalisés par les utilisateurs à la fin de chaque série de question 
- Tableaux avec l'historique des scores réalisés (utilisateur, mode, nombre de question, nombre de question juste, taux de réussite, temps de réponse)
